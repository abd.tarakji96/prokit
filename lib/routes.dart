import 'package:flutter/material.dart';
import 'package:prokit/main/model/AppModel.dart';
import 'package:prokit/main/screens/ProKitScreenListing.dart';

Map<String, WidgetBuilder> routes() {
  return <String, WidgetBuilder>{
    ProKitScreenListing.tag: (context) => ProKitScreenListing(ProTheme()),
  };
}
